// import finiteAutomata from "./components/finiteAutomata";
// import CheckFiniteAutomata from "./components/CheckFiniteAutomata";
// import CheckStringAcceptFA from "./components/CheckStringAcceptFA";
// import ConvertNFAtoDFA from "./components/ConvertNFAtoDFA";
// import MinimizeDFA from "./components/MinimizeDFA";
import Header from "./layouts/Header";
import Auth from "./layouts/Auth";
import HomePage from "./layouts/HomePage";
import { Route, Routes } from "react-router-dom";
// import FiniteAutomata from "./components/FiniteAutomata";
import TestDesignFA from "./components/TestDesignFA";
import History from "./Backend/History";


const App = () => {
  return (
    <div className='w-full h-[100vh]'>
      {/* //call header to app */}
     <Header/>
     <div>
     <Routes>
      <Route path="/" element={<Auth/>}/>
      <Route path="/home" element={<HomePage/>}/>
      <Route path="/history" element={<History/>}/>
      {/* <Route path="/" element={<TestDesignFA/>}/> */}
     </Routes>
     {/* <TestDesignFA/> */}
     {/* <CheckDFA/> */}
     </div>
    </div>
  )
}

export default App