import { initializeApp } from "firebase/app";
import { GoogleAuthProvider, getAuth } from "firebase/auth";
import {getFirestore} from "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyAJmH7CVk3wDzvxVWiiowHPsycZE2CUbYg",
  authDomain: "automata-a6642.firebaseapp.com",
  projectId: "automata-a6642",
  storageBucket: "automata-a6642.appspot.com",
  messagingSenderId: "1086441806650",
  appId: "1:1086441806650:web:d6c37745996901e1e5ce03",
  measurementId: "G-3MTSV6DX2X"
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const googleProvider = new GoogleAuthProvider();
const db = getFirestore(app); 
export {db};