import React, { useState, useEffect } from 'react';
import { doc, collection, deleteDoc, getDocs } from 'firebase/firestore';
import { db } from './Firebase';
import { Button } from '../components/Button';
import Swal from 'sweetalert2';

const History = ({ onSave }) => {
  const [DataList, setDataList] = useState([]);

  const faCollection = collection(db, "finiteAutomata");

  useEffect(() => {
    const getDataList = async () => {
      try {
        const data = await getDocs(faCollection);
        const filterData = data.docs.map((doc) => ({
          ...doc.data(),
          id: doc.id,
        }));
        setDataList(filterData);
      } catch (error) {
        console.log(error);
      }
    };

    getDataList();
  }, []);

  const deleteData = async (id) => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const dataDoc = doc(db, "finiteAutomata", id);
          await deleteDoc(dataDoc);
          // Update the local state to remove the deleted item from the list
          setDataList(prevDataList => prevDataList.filter(item => item.id !== id));
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );
        } catch (error) {
          console.error("Error deleting document: ", error);
          Swal.fire(
            'Error!',
            'There was a problem deleting the document.',
            'error'
          );
        }
      }
    });
  };

  return (

    <div className='w-full h-screen flex justify-center ' >
    <div className="flex flex-col w-[90%] mt-5">
    <p className='mt-5 text-3xl flex font-bold text-indigo-900'>History</p>
    {DataList.map((dataLis) => (
        <div className='flex flex-col shadow-xl p-5 mt-5 rounded border' 
             key={dataLis.id}
        >
          <p>Q = &#123;{dataLis.states}&#125;</p>
          <h1>X = &#123;{dataLis.symbol}&#125;</h1>
          <p>Start State: &#123;{dataLis.initialState}&#125;</p>
          <p>Final State: &#123;{dataLis.finalState}&#125;</p>
          <p>Transition:</p>
          <ul>
            {dataLis.transition && Object.entries(dataLis.transition).map(([key, value], index) => {
              const [fromState, symbol] = key.split(',');
              return (
                <li key={index}>
                  From: <span className='text-[#F58C29] font-bold'>{fromState}</span> , Symbol: <span className='text-[#15284C] font-bold'>{symbol}</span> , To: <span className='text-[#F58C29] font-bold'>{value}</span>
                </li>
              );
            })}
          </ul>
          <div className=" flex justify-end">
            <Button  
            className="bg-red-600 text-white p-5"
            onClick={() => deleteData(dataLis.id)}
            >
                Delete
            </Button>
          </div>
        </div>
        
      ))}
    </div>
    </div>
  );
};

export default History;
