import PropTypes from 'prop-types';

export const Button = ({children, className, icon, onClick, ...props}) => (
    <button 
        className={`border-2 border-solid-grey tracking-wide h-[46px] flex items-center px-3 mb-3 rounded shadow-md cursor-pointer  ${className}`}
        onClick={onClick}
        {...props}
    >
        {children}
    </button>
);

Button.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    onClick: PropTypes.func,
    icon: PropTypes.element,
};
