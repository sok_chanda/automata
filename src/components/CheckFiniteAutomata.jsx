import React from "react";
import { Button } from "./Button";

const CheckFiniteAutomata = ({ states, symbol, startState, finalState, transition, epsilon, setFaType }) => {
  
  

  const checkFAType = () => {
    let isNFA = false;

    // Check if transitions include ∅ or multiple next states
    for (let key in transition) {
      if (transition[key].includes("∅")) { 
        isNFA = true;
        break;
      }
      const result = transition[key].split(",");
      if (result.length > 1) {
        isNFA = true;
        break;
      }
    }

    // Check for epsilon transitions
    if (epsilon) {
      isNFA = true;
    }

    // Set the FA type
    setFaType(isNFA ? 'NFA' : 'DFA');
  };

  return (
    <div className="mt-3">
      <Button className="bg-[#15284C] justify-center text-white w-[120px] h-[40px]" onClick={checkFAType}>Check</Button>
    </div>
  );
};

export default CheckFiniteAutomata;
