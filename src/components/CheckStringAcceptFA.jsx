import { useState } from "react";
import { Button } from "./Button";

const CheckStringAcceptFA = ({faType, startState, finalState, transition }) => {
  const [inputString, setInputString] = useState("");
  
  const checkStringDfa = () => {
    if (faType !== "DFA") return false;

    if (inputString === "") {
      alert("Please input string");
      return;
    }

    const stringArray = inputString.split("");
    let currentState = startState;

    console.log("Initial State: ", currentState);
    console.log("Transitions: ", transition);
    console.log("Final States: ", finalState);
    console.log("Input String: ", inputString);

    // condition to find the flow of the transition to find string accept or not from initial state till final state
    for (let character of stringArray) {
      console.log("Current Character: ", character);
      const transitionKey = `${currentState},${character}`;

      if (transition.hasOwnProperty(transitionKey)) {
        currentState = transition[transitionKey];
        console.log("Transition found, new state: ", currentState);
      } else {
        console.log("No transition found for character: ", character);
        currentState = null;
        break;
      }
    }

    const isAccepted = finalState.includes(currentState);
    console.log("isAccepted",isAccepted);

    if (isAccepted) {
      alert("String Accepted");
    } else {
      alert("String Not Accepted DFA");
    }


  };

  const checkStringNFA = () => {
    if (faType !== "NFA") return false;

    const stringArray = inputString.split("");

    if (inputString === "") {
      alert("Please enter a string");
      return;
    }

    console.log("=================NFA==================");

    let nfaStates = new Set([startState]); //nfaStates = q0

    stringArray.forEach((character) => {
      console.log("-------character-------: ", character);
      console.log("start state: ", [...nfaStates]);

      let tempStates = new Set(); // let new temporary state to store in set when met condition that must to delete it will deleted
      console.log("tempState: ",tempStates);

      nfaStates.forEach((state) => {
        const transitionKey = `${state},${character}`;
        if (transition.hasOwnProperty(transitionKey)) {
          const newStates = transition[transitionKey].split(",");
          newStates.forEach((newState) => {
            if (newState !== "∅") {
              tempStates.add(newState);
            }
          });
        }
      });

      // Handle epsilon transitions
      let epsilonClosure = new Set(tempStates);
      tempStates.forEach((state) => {
        const epsilonKey = `${state},ε`;
        if (transition.hasOwnProperty(epsilonKey)) {
          const newStates = transition[epsilonKey].split(",");
          newStates.forEach((newState) => {
            if (newState !== "∅" && !epsilonClosure.has(newState)) {
              epsilonClosure.add(newState);
            }
          });
        }
      });

      nfaStates = epsilonClosure;
    });

    console.log("Final states: ", [...nfaStates]);

    const isAccepted = [...nfaStates].some((state) => finalState.includes(state));

    if (isAccepted) {
      alert("This string is accepted");
    } else {
      alert("This string is not accepted");
    }
  };

  return (
    <div className="mt-3">
      <input
        type="text"
        onChange={(e) => setInputString(e.target.value)}
        value={inputString}
        placeholder="Please input a string..."
        className="py-4 outline-none w-full"
      />
      <hr />
      <Button
        onClick={() => (faType === "DFA" ? checkStringDfa():checkStringNFA())}
        className="bg-[#15284C] justify-center mt-2 text-white w-[130px] h-[40px]"
      >
        Check String
      </Button>
    </div>
  );
};

export default CheckStringAcceptFA;
