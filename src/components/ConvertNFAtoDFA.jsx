import React, { useState } from "react";
import { Button } from "./Button";
import Swal from 'sweetalert2';
const ConvertNFAtoDFA = ({faData,faType}) => {
  const [convertedDFA, setConvertedDFA] = useState(null);

  const convertNFAtoDFA = () => {
    if (!faData) {
      alert("No NFA data available for conversion.");
      return;
    }

    // alert 
    if (faType !== "NFA") {
      Swal.fire({
        icon: 'info',
        title: 'It is already a DFA',
        text: 'The finite automaton you provided is already a DFA.',
        confirmButtonText: 'OK'
      });
      return;
    }

    // Step 1: Initialize
    const initialState = faData.startState;
    const initialDFAState = epsilonClosure(new Set([initialState]));

    // Step 2: ε-Closure
    function epsilonClosure(states) {
      let closure = new Set(states);
      let stack = [...states];

      while (stack.length > 0) {
        let currentState = stack.pop();
        const epsilonKey = `${currentState},ε`;

        if (faData.transition.hasOwnProperty(epsilonKey)) {
          const newStates = faData.transition[epsilonKey].split(',');
          newStates.forEach(newState => {
            if (!closure.has(newState)) {
              closure.add(newState);
              stack.push(newState);
            }
          });
        }
      }

      return closure;
    }

    // Step 3: Process States
    let dfaStates = [initialDFAState];
    let dfaTransitions = {};
    let queue = [initialDFAState];
    let stateIndexMap = new Map();
    stateIndexMap.set(setToString(initialDFAState),0);


    while (queue.length > 0) {
      let currentDFAState = queue.shift();
      faData.symbol.split(',').forEach(symbol => {
        let newState = new Set();

        currentDFAState.forEach(state => {
          const transitionKey = `${state},${symbol}`;
          if (faData.transition.hasOwnProperty(transitionKey)) {
            const reachableStates = faData.transition[transitionKey].split(',');
            reachableStates.forEach(reachableState => {
              newState.add(reachableState);
            });
          }
        });

        if (newState.size > 0) {
          const epsilonClosureSet = epsilonClosure(newState);
          const closureString = setToString(epsilonClosureSet);

          if(!stateIndexMap.has(closureString)){
            stateIndexMap.set(closureString, dfaStates.length);
            dfaStates.push(epsilonClosureSet);
            queue.push(epsilonClosureSet);
          }

          const fromStateIndex = stateIndexMap.get(setToString(currentDFAState));
          const toStateIndex   = stateIndexMap.get(closureString);
          const fromState      = `q${fromStateIndex}' = {${setToString(currentDFAState)}}`;
          const toState        = `q${toStateIndex}'   = {${closureString}}`;

          dfaTransitions[
            `q${fromStateIndex}',-->${symbol} = {${closureString}}`
          ] = `q${toStateIndex}'`;
        }
      });
    }

    // Step 4: Handle ε-Transitions
    function setToString(set) {
      return Array.from(set).sort().join(',');
    }

    function setEquals(set1, set2) {
      if (set1.size !== set2.size) return false;
      for (let item of set1) {
        if (!set2.has(item)) return false;
      }
      return true;
    }

    // Step 5: Finalize
    const dfaData = {
      states: dfaStates.map((state, index) => `q${index}' = {${setToString(state)}}`),
      symbol: faData.symbol,
      startState: setToString(initialDFAState),
      finalState: dfaStates
      .map((state,index) => {
        if(Array.from(state).some((s) => faData.finalState.includes(s))){
          return `q${index}'`;
        }
        return null;
      })
      .filter((state) => state !== null),
      transition: dfaTransitions
    };

    setConvertedDFA(dfaData);
    // setDfaData(dfaData);
  };

  return (
    <div className="mt-3">
      <Button
        className="bg-[#15284C] justify-center text-white w-[120px] h-[40px]"
        onClick={convertNFAtoDFA}
      >
        Convert
      </Button>
      {convertedDFA && (
        <div className="mt-3">
          <h2 className="font-semibold text-lg mb-2">Converted DFA</h2>
          <pre>{JSON.stringify(convertedDFA, null, 2)}</pre>
          {/* Render DFA visualization or details here */}
        </div>
      )}
    </div>
  );
};

export default ConvertNFAtoDFA;
