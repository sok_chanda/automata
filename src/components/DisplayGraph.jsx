import React from "react";
import Graphviz from "graphviz-react";

const DisplayGraph = ({ dfa }) => {
  if (!dfa) {
    return <p>No FA data to display.</p>;
  }

  const generateDotCode = () => {
    let dotCode = 'digraph finite_state_machine {\n';
    dotCode += '  fontname="Helvetica,Arial,sans-serif"\n';
    dotCode += '  node [fontname="Helvetica,Arial,sans-serif"]\n';
    dotCode += '  edge [fontname="Helvetica,Arial,sans-serif"]\n';
    dotCode += '  rankdir=LR;\n';

    // Define an invisible start node
    dotCode += '  "" [shape = none, label = ""];\n';
    dotCode += `  "" -> ${dfa.initialState};\n`;

    // Define accepting states
    dotCode += '  node [shape = doublecircle]; ';
    dfa.acceptingStates.forEach(state => {
      if (state !== dfa.initialState) {
        dotCode += `${state} `;
      }
    });
    dotCode += ';\n';

    // Define the initial state (which might also be a final state)
    if (dfa.acceptingStates.includes(dfa.initialState)) {
      dotCode += `  ${dfa.initialState} [shape = doublecircle];\n`;
    } else {
      dotCode += `  ${dfa.initialState} [shape = circle];\n`;
    }

    // Define regular states
    dotCode += '  node [shape = circle];\n';

    // Define edges
    for (let [key, value] of Object.entries(dfa.transition)) {
      const [state, symbol] = key.split(',');
      const targets = value.split(',');
      targets.forEach(target => {
        dotCode += `  ${state} -> ${target} [label = "${symbol}"];\n`;
      });
    }

    dotCode += '}';
    // console.log("Generated DOT Code:", dotCode); // Add logging to check DOT code
    return dotCode;
  };

  // Adding a try-catch block to handle potential errors
  const renderGraph = () => {
    try {
      return <Graphviz dot={generateDotCode()} options={{ height: '300px' }} />;
    } catch (error) {
      console.error("Error rendering Graphviz:", error);
      return <p>Failed to render graph. Check console for details.</p>;
    }
  };

  return (
    <div className="mt-10">
      <h2 className="font-semibold text-2xl text-center">Graph</h2>
      <div className="flex justify-center">
        {renderGraph()}
      </div>
    </div>
  );
};

export default DisplayGraph;
