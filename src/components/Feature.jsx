import React, { useState } from "react";
import { Button } from "./Button";
import DisplayGraph from "./DisplayGraph";
import CheckFiniteAutomata from "./CheckFiniteAutomata";
import CheckStringAcceptFA from "./CheckStringAcceptFA";
import ConvertNFAtoDFA from "./ConvertNFAtoDFA";



const Feature = ({ faData,inputString,setInputString }) => {
  const [faType, setFaType] = useState('');
  //const [DfaToNfa,setDfaToNfa] = useState(null);
 
  return (
    <div className="h-screen pt-10 px-5">
      <h2 className="text-3xl font-bold tracking-wider text-red-700 mb-5">Features</h2>
      <div className="p-5 rounded-lg bg-[#F7F7F7]">
        <div className="flex flex-col gap-2">
          <label htmlFor="identify" className="font-semibold">Identify That DFA or NFA: </label>
          {faType && 
          <h2>This is <strong>{faType}</strong></h2>
          }
          <CheckFiniteAutomata 
            states={faData.states || ''}
            symbol={faData.symbol || ''}
            startState={faData.startState || ''}
            finalState={faData.finalState || []}
            transition={faData.transition || {}}
            epsilon={faData.epsilon || false}
            setFaType={setFaType}
          />
        </div>
        <div className="flex flex-col gap-2 mt-2">
          <label htmlFor="testString" className="font-semibold">Test String Accept: </label>
          <CheckStringAcceptFA
            faData={faData}
            states={faData.states || ''}
            symbol={faData.symbol || ''}
            startState={faData.startState || ''}
            finalState={faData.finalState || []}
            transition={faData.transition || {}}
            inputString={inputString}
            setInputString={setInputString}
            faType={faType}
          />
        </div>
        <div className="flex flex-col gap-2 mt-2">
          <ConvertNFAtoDFA
            faData={faData}
            states={faData.states || ''}
            symbol={faData.symbol || ''}
            startState={faData.startState || ''}
            finalState={faData.finalState || []}
            transition={faData.transition || {}}
          />
        </div>
       
      </div>
      {faData && (
          <div>
              <DisplayGraph dfa={faData} />
          </div>
        )}
    </div>
  );
};  

export default Feature;
