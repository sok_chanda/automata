import React from 'react';
import Multiselect from 'multiselect-react-dropdown';

const TableTransition = ({ states, symbol, startState, finalState, handleTransitionChange, transition, epsilon }) => {
  let symbolArr = symbol.split(',');
  const statesArr = states.split(',');

  if (epsilon) {
    symbolArr = [...symbolArr, 'ε'];
  }

  const handleOnSelect = (e, state, symbol) => {
    const value = e.map(item => item.name).join(',');
    handleTransitionChange(state, symbol, value);
  };

  // Prepare selectData with an additional "empty" state
  const selectData = statesArr.map(state => ({ name: state, id: state }));
  selectData.push({ name: "∅", id: "∅" });

  return (
    <table className="table-auto w-full mt-5 border-collapse border border-gray-400">
      <thead>
        <tr>
          <th className="border border-blue-500 px-4 py-2">State</th>
          {symbolArr.map((symbol, index) => (
            <th className="border border-blue-500 px-4 py-2" key={index}>
              {symbol}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {statesArr.map((state, stateIndex) => (
          <tr key={stateIndex}>
            <td className="border border-blue-500 px-3 py-2 font-bold text-center">
              {state === startState && <span className='text-blue-500'>&#10145;</span>}
              {finalState.includes(state) && <span className='text-red-500'>*</span>}
              {state}
            </td>
            {symbolArr.map((symbol, symbolIndex) => (
              <td key={symbolIndex} className="border border-blue-500 px-3 py-2">
                <Multiselect
                  options={selectData}
                  displayValue='name'
                  onSelect={(e) => handleOnSelect(e, state, symbol)}
                  onRemove={(e) => handleOnSelect(e, state, symbol)}
                  selectedValues={
                    transition[`${state},${symbol}`]
                      ? transition[`${state},${symbol}`].split(',').map(s => ({ name: s, id: s }))
                      : []
                  }
                  placeholder="Select state"
                />
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TableTransition;
