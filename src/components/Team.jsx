// src/components/Team.js
import React from 'react';
import TeamMember from './TeamMember';
import './Team.css'

const teamMembers = [
    { name: 'Sok Sothy', role: 'Developer', image: 'src/assets/5.png', bio: 'Alice is a skilled developer with 5 years of experience in web development.' },
    { name: 'Srun Srorn', role: 'Designer', image: 'src/assets/1.png', bio: 'Bob is a creative designer specializing in UI/UX design.' },
    { name: 'Sok Chanda', role: 'Project Manager', image: 'src/assets/6.png', bio: 'Charlie is an experienced project manager who ensures everything runs smoothly.' },
    { name: 'Ry Dola', role: 'QA Engineer', image: 'src/assets/3.png', bio: 'Diana is a detail-oriented QA engineer.' },
    { name: 'Sok Makara', role: 'DevOps Engineer', image: 'src/assets/4.png', bio: 'Eve is a DevOps engineer with expertise in cloud infrastructure.' },
    { name: 'Anya', role: 'DevOps Engineer', image: 'src/assets/7.png', bio: 'Eve is a DevOps engineer with expertise in cloud infrastructure.' },
];

const Team = () => {
    return (
        <div className="py-12 px-6 w-full max-w-6xl mx-auto mt-0"> {/* Adjust the mt-24 to match your header height */}
            <h2 className="text-5xl font-bold text-center mb-8">OUR TEAM</h2>
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8">
                {teamMembers.map((member, index) => (
                    <TeamMember
                        key={index}
                        name={member.name}
                        role={member.role}
                        image={member.image}
                        bio={member.bio}
                    />
                ))}
            </div>
        </div>
    );
};

export default Team;
