// src/components/TeamMember.js
import React, { useState } from 'react';
import './TeamMember.css';

// eslint-disable-next-line react/prop-types
const TeamMember = ({ name, role, image, bio }) => {
    const [showBio, setShowBio] = useState(false);

    return (
        <div className="bg-white shadow-lg rounded-lg p-6 flex flex-col items-center team-member" onClick={() => setShowBio(!showBio)}>
            <div className="burning-avatar">
                <img src={image} alt={name} />
            </div>
            <h3 className="text-3xl font-bold mt-3">{name}</h3>
            <p className="text-gray-700">{role}</p>
            {showBio && <p className="mt-4 text-gray-600">{bio}</p>}
        </div>
    );
};

export default TeamMember;
