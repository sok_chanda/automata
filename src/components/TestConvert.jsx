import React, { useState } from 'react';
import { Button } from './Button';

const TestConvert = ({ faType, startState, finalState, transition, symbol, states, setDfaToNfa }) => {

  const convertNFAtoDFA = () => {
    // Check if the input FA is DFA or NFA
    if (faType !== 'NFA') return false;

    // Convert transition object to array of objects
    const transitionArray = Object.keys(transition).map(key => ({
      state: key.split(',')[0],
      symbol: key.split(',')[1],
      transition: transition[key]
    }));

    let nfaStates = [startState];
    let dfaStates = [];

    // Handle epsilon transition in the NFA
    transitionArray.forEach((trans) => {
      for (let i = 0; i < nfaStates.length; i++) {
        if (nfaStates[i] === trans.state && trans.symbol === "ε") {
          if (trans.transition.includes(",")) {
            const result = trans.transition.split(",");
            nfaStates = [...nfaStates, ...result];
          } else if (trans.transition !== "∅") {
            nfaStates = [...nfaStates, trans.transition];
          }
        }
      }
    });

    // Define A'
    dfaStates.push({
      fromNfaStates: nfaStates,
      name: "q0'",
      isStart: true,
      isEndState: false,
      nfaToDfaTransitions: [],
      transition: [],
    });

    // Process each DFA state for each character
    for (let i = 0; i < dfaStates.length; i++) {
      // Iterate over each character (symbol)
      symbol.forEach((character) => {
        
        let tempDfaStates = [];

        // Iterate over each state in current DFA state's fromNfaStates
        for (let j = 0; j < dfaStates[i].fromNfaStates.length; j++) {
          // Iterate over each transition in transitionArray
          for (let n = 0; n < transitionArray.length; n++) {
            const trans = transitionArray[n];
            // Check for matching state and character in the transition object
            if (
              dfaStates[i].fromNfaStates[j] === trans.state &&
              character === trans.symbol
            ) {
              if (trans.transition.includes(",")) {
                let result = trans.transition.split(",");
                result = result.filter((item) => !tempDfaStates.includes(item));
                tempDfaStates = [...tempDfaStates, ...result];
              } else if (trans.transition !== "∅") {
                if (!tempDfaStates.includes(trans.transition)) {
                  tempDfaStates = [...tempDfaStates, trans.transition];
                }
              }

              // Handle epsilon transition for resulting states
              transitionArray.forEach((epsTrans) => {
                for (let m = 0; m < tempDfaStates.length; m++) {
                  if (
                    tempDfaStates[m] === epsTrans.state &&
                    epsTrans.symbol === "ε"
                  ) {
                    if (epsTrans.transition.includes(",")) {
                      let result = epsTrans.transition.split(",");
                      result = result.filter((item) => !tempDfaStates.includes(item));
                      tempDfaStates = [...tempDfaStates, ...result];
                    } else if (epsTrans.transition !== "∅") {
                      if (!tempDfaStates.includes(epsTrans.transition)) {
                        tempDfaStates = [
                          ...tempDfaStates,
                          epsTrans.transition,
                        ];
                      }
                    }
                  }
                }
              });
            }
          }
        }

        // Check if the state already exists in dfaStates
        let isExist = dfaStates.some((state) => {
          const sortedArr1 = state.fromNfaStates.slice().sort();
          const sortedArr2 = tempDfaStates.slice().sort();
          return sortedArr1.every((element, index) => element === sortedArr2[index]);
        });

        let dfaTransition = "";
        if (!isExist) {
          const isEndState = tempDfaStates.some((element) => finalState.includes(element));

          dfaStates.push({
            fromNfaStates: tempDfaStates,
            name: "q" + dfaStates.length + "'",
            isStart: false,
            isEndState: isEndState,
            nfaToDfaTransitions: [],
            transition: [],
          });

          dfaTransition = "q" + (dfaStates.length - 1) + "'";
        } else {
          dfaTransition = dfaStates.find((state) => {
            const sortedArr1 = state.fromNfaStates.slice().sort();
            const sortedArr2 = tempDfaStates.slice().sort();
            return sortedArr1.every((element, index) => element === sortedArr2[index]);
          }).name;
        }

        dfaStates[i].transition.push({
          character: character,
          transition: dfaTransition,
        });

        dfaStates[i].nfaToDfaTransitions.push({
          character: character,
          transition: tempDfaStates.join(","),
        });

        if (i == 0) {
          const isEnd = dfaStates[0].fromNfaStates.some((element) => finalState.includes(element));
          dfaStates[0].isEndState = isEnd;
        }
      });
    }

    setDfaToNfa(dfaStates);
    console.log("DfaToNfa:", dfaStates);
  };

  return (
    <div className='mt-3'>
      <Button
        onClick={convertNFAtoDFA}
        className="bg-[#15284C] justify-center mt-2 text-white w-[130px] h-[40px]"
      >
        Convert
      </Button>
    </div>
  )
}

export default TestConvert;
