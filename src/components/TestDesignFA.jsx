import React, { useEffect, useState } from "react";
import Multiselect from "multiselect-react-dropdown";
import TableTransition from "./TableTransition";
import { Button } from "./Button";
import { db } from "../Backend/Firebase";
import { addDoc, collection } from "firebase/firestore";



const TestDesignFA = ({ onSaveFA }) => {



  const [symbol, setSymbol] = useState('');
  const [states, setStates] = useState('');
  const [startState, setStartState] = useState('');
  const [finalState, setFinalState] = useState([]);
  const [transition, setTransition] = useState({});
  const [epsilon, setEpsilon] = useState(false);

  const handleStartStateChange = (selectedList) => {
    setStartState(selectedList.map(item => item.name)[0] || '');
  };

  const handleFinalStateChange = (selectedList) => {
    setFinalState(selectedList.map(item => item.name));
  };

  const handleTransitionChange = (state, symbol, value) => {
    setTransition(prevTransitions => ({
      ...prevTransitions,
      [`${state},${symbol}`]: value
    }));
  };

  const handleEpsilon = (event) => {
    setEpsilon(event.target.value === "true");
  };
  
  const handleSaveFA = async () => {
    const faData = {
      symbol,
      states,
      startState,
      finalState,
      transition,
      epsilon,
      initialState: startState,
      acceptingStates: finalState,
    };

    try {
      await addDoc(collection(db, 'finiteAutomata'), faData);
      alert('FA Saved Successfully!');
      onSaveFA(faData); // Pass the FA data to the parent component
    } catch (error) {
      console.error('Error adding document', error);
      alert('Error Save FA!');
    }
  };



  return (
    <div className="h-screen py-10 px-5">
      <div className="flex flex-col">
        <div className="flex justify-between">
          <h2 className="text-3xl font-bold tracking-wider text-red-700 mb-5">Design Finite Automata</h2>
          <Button className="bg-yellow-500 w-[100px] font-semibold text-white justify-center" onClick={handleSaveFA}>Save FA</Button>
        </div>
        <div className="p-5 rounded-lg bg-[#F7F7F7]">
          <div className="flex flex-col gap-2">
            <label htmlFor="symbols" className="font-semibold">Define Symbols:</label>
            <input 
              type="text" 
              name="symbol"
              placeholder="a, b or 1, 0"
              className="px-2 py-2 rounded border-2 outline-none border-gray-300 mb-5"
              value={symbol}
              onChange={(e) => setSymbol(e.target.value)}
            />
          </div>
          <div className="flex flex-col gap-2">
            <label htmlFor="states" className="font-semibold">Define States:</label>
            <input 
              type="text" 
              name="states"
              placeholder="q0, q1, q2"
              className="px-2 py-2 rounded border-2 outline-none border-gray-300 mb-5"
              value={states}
              onChange={(e) => setStates(e.target.value)}
            />
          </div>
          <div className="flex flex-col gap-2 mb-5">
            <label htmlFor="startState" className="font-semibold">Define Start State:</label>
            <Multiselect
              options={states.split(',').map(state => ({name: state, id: state}))}
              displayValue="name"
              selectedValues={startState ? [{name: startState, id: startState}] : []}
              onSelect={handleStartStateChange}
              onRemove={handleStartStateChange}
              singleSelect
              placeholder="Select your start state..."
            />
          </div>
          <div className="flex flex-col gap-2 mb-5">
            <label htmlFor="finalState" className="font-semibold">Define Final States:</label>
            <Multiselect
              options={states.split(',').map(state => ({name: state, id: state}))}
              displayValue="name"
              selectedValues={finalState.map(state => ({ name: state, id: state }))}
              onSelect={handleFinalStateChange}
              onRemove={handleFinalStateChange}
              placeholder="Select your final states..."
            />
          </div>
          <div className="flex flex-col gap-2 mb-5">
            <label htmlFor="epsilon" className="font-semibold">Define Epsilon:</label>
            <select
              defaultValue="false"
              name="epsilon"
              onChange={handleEpsilon}
              className="px-2 py-2 rounded border-2 outline-none border-gray-300 mb-5"
            >
              <option value="false">No Epsilon Transition</option>
              <option value="true">Include Epsilon Transition</option>
            </select>
          </div>
        </div>
        <div className="px-5 h-[550px]">
          {symbol && states && (
            <div className="flex justify-center mt-10 flex-col">
              <h2 className="text-2xl text-center font-bold tracking-wide text-indigo-600 mb-5">Table Transition Of Automata</h2>
              <TableTransition
                states={states}
                symbol={symbol}
                startState={startState}
                finalState={finalState}
                handleTransitionChange={handleTransitionChange}
                transition={transition}
                epsilon={epsilon}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default TestDesignFA;
