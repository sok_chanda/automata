import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { auth, googleProvider } from '../Backend/Firebase';
import { Button } from "../components/Button";
import { createUserWithEmailAndPassword, signInWithPopup, signOut} from 'firebase/auth';
import picTeacher from "../assets/pic_teacher.jpg"

export const Auth = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
console.log(auth?.currentUser?.email);
  const signIn = async (event) => {
    event.preventDefault();
    try {
      await createUserWithEmailAndPassword(auth, email, password);
      console.log("User signed up successfully!");
      navigate("/home");
    } catch (err) {
      console.error("Error signing up:", err.message);
      navigate("/home");
    }
  };

  const signWithGoogle = async (event) => {
    event.preventDefault();
    try {
      const result = await signInWithPopup(auth, googleProvider);
      console.log("User signed in with Google:", result.user);
      navigate("/home");
    } catch (err) {
      console.error("Error signing in with Google:", err.message);
    }
  };
  // const logout = async (event) => {
  //   event.preventDefault();
  //   try {
  //     await signOut(auth);
  //   }catch(err){
  //     console.error(err);
  //   }
  // };

  return (
    <div className="flex flex-col justify-center items-center mt-5">
      <h1 className="font-bold text-3xl ">Unauthenticated, Please Create An Account!</h1>
      <img
      className="w-[150px] h-[150px] rounded-full mt-5"
      src={picTeacher} alt="" />
      <div className="flex flex-col text-xl font-semibold mt-3 justify-center items-center">
        <h2>Welcome to Automata Simulation</h2>
        <h2>Project: Automata</h2>
        <h2>Lecture: Dona Valy</h2>
      </div>
      <form className="mt-3 flex flex-col">
        {/* <label htmlFor="email" className="mb-2 text-lg">Enter Email</label> */}
        <input 
          type="email" 
          id="email"
          placeholder="Enter your email..." 
          className="border border-gray-300 p-2 rounded mb-4"
          onChange={(e) => setEmail(e.target.value)}
        />
        {/* <label htmlFor="password" className="mb-2 text-lg">Enter Password</label> */}
        <input 
          type="password" 
          id="password"
          placeholder="Enter your password..." 
          className="border border-gray-300 p-2 rounded mb-4"
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button className="justify-center bg-[#15284C] text-white" onClick={signIn}>Continue</Button>
        <Button className="" onClick={signWithGoogle}>
          <img className="w-[39px] mx-1" src="https://cdn1.iconfinder.com/data/icons/google-s-logo/150/Google_Icons-09-512.png" alt="" />
          Continue with Google
        </Button>
        <Button className="" >
          <img className="w-[27px] mx-2" src="https://upload.wikimedia.org/wikipedia/commons/6/6c/Facebook_Logo_2023.png" alt="" />
           Continue with facebook
          </Button>
        {/* <Button className="bg-slate-500" onClick={logout}> 
          Log out
        </Button> */}
      </form>
    </div>
  );
};

export default Auth;
