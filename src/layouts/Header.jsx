// src/components/Header.js
import { useEffect, useState } from "react";
import logo from "../assets/logo.jpg";
import { useNavigate } from "react-router-dom";
import { auth } from "../Backend/Firebase";
import { onAuthStateChanged, signOut } from "firebase/auth";
import { IoIosLogOut } from "react-icons/io";

const Header = () => {
  const [user, setUser] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const unSubscribe = onAuthStateChanged(auth, (currentUser) => {
      setUser(currentUser);
    });
    return () => unSubscribe();
  }, []);

  const logOut = async (event) => {
    try {
      event.preventDefault();
      await signOut(auth);
      navigate("/");
    } catch (err) {
      console.log(err);
    }
  };

  return (
      <div className="w-full h-[100px] px-6 bg-white shadow-lg sticky top-0 flex justify-between items-center z-50">
        <img src={logo} alt="" className="h-[78px]" />
        <div className="flex items-center">
          {user && (
              <button
                  className="flex text-xl mr-5 font-semibold text-gray-500 items-center border-none shadow-none"
                  onClick={logOut}
              >
                <IoIosLogOut className="mr-1" size={30} />
                LogOut
              </button>
          )}
          <h1 className="font-bold text-xl tracking-wide text-indigo-800">
            {user ? (
                <span>
              <span className="text-indigo-800">Welcome, </span>
              <span className="text-green-500">{user.displayName}</span>
            </span>
            ) : (
                "Automata"
            )}
        </h1>
      </div>
            
      </div>
  );
};

export default Header;
