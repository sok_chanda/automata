import React, { useState } from "react";
import Feature from "../components/Feature";
import TestDesignFA from "../components/TestDesignFA";

const HomePage = () => {
  const [faData, setFaData] = useState(null);

  const handleSaveFA = (data) => {
    setFaData(data);
  };

  return (
    <div className="w-full h-screen flex justify-between">
      <div className="w-[50%] ">
        <TestDesignFA onSaveFA={handleSaveFA} />
      </div>
      <div className="w-[50%] ">
        {faData && <Feature faData={faData} />}
      </div>
    </div>
  );
};

export default HomePage;
